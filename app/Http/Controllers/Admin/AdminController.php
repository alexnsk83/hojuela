<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Helpers\CheckAvatar;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::all();
        $title = trans('site.title.admin');

        foreach ($users as $user) {
            $user['avatar'] = CheckAvatar::isAvatarExists($user['avatar']);
        }

        return view('admin.pages.index', compact('users', 'title'));
    }
}

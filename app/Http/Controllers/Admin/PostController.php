<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Post;
use Intervention\Image\Facades\Image;


class PostController extends Controller
{
    /**
     * Отображает список статей
     * @param bool $trashed
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAll($trashed = false)
    {
        $posts = $trashed ? Post::latest()->onlyTrashed()->get() : Post::latest()->get();
        $title = trans('site.title.posts');

        return view('admin.pages.post_list', compact('posts', 'title', 'trashed'));
    }

    /**
     * Отображает только удалённые статьи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showTrashed()
    {
        return $this->showAll(true);
    }

    /**
     * Форма создания статьи
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $title = trans('site.title.post_new');
        return view('admin.pages.post_add', compact('title'));
    }

    /**
     * Сохранение статьи
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addPost()
    {
        $post = new Post();

        $this->validate($this->request, [
            'caption' => 'required|unique:posts|min:5',
            'detail' => 'required|min:5',
            'image' => 'file|image',
        ]);

        $announce = explode('.', $this->request->detail);
        $announce = substr($announce[0], 0, 100);
        $announce = str_replace(['<pre>', '</pre>'], '', $announce);
        $announce = trim($announce);

        if(is_file($_FILES['image']['tmp_name'])){
            Input::file('image')->move('assets/images/uploads/posts/', $_FILES['image']['name']);
            $img = Image::make(public_path() . '/assets/images/uploads/posts/' . $_FILES['image']['name']);
            $img->fit(640, 480)
                ->save(public_path() . '/assets/images/uploads/posts/' . $_FILES['image']['name']);
            $post->image = '/assets/images/uploads/posts/' . $_FILES['image']['name'];
        }

        $post->caption = $this->request->caption;
        $post->announce = json_encode($announce);
        $post->detail = $this->request->detail;
        $post->user_id = Auth::id();
        $post->is_visible = $this->request->is_visible;
        $post->show_comments = $this->request->show_comments;
        $post->commentable = $this->request->commentable;

        $post->save();

        return redirect()->route('admin.post.all')->with('message', trans('messages.post_created'));
    }

    /**
     * Форма редактирования статьи
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::withTrashed()
            ->where('id', $id)->first();
        $title = trans('site.title.post_edit');

        return view('admin.pages.post_add', compact('post', 'title'));
    }

    /**
     * Сохранение изменений в статье
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editPost($id)
    {
        $this->validate($this->request, [
            'caption' => 'required|min:5',
            'detail' => 'required|min:5',
            'image' => 'file|image',
        ]);

        $announce = explode('.', $this->request->detail);
        $announce = substr($announce[0], 0, 100);
        $announce = str_replace(['<pre>', '</pre>'], '', $announce);
        $announce = trim($announce);

        $image = Post::withTrashed()
            ->find($id)->image;

        if(is_file($_FILES['image']['tmp_name'])){
            Input::file('image')->move('assets/images/uploads/posts/', $_FILES['image']['name']);
            $img = Image::make(public_path() . '/assets/images/uploads/posts/' . $_FILES['image']['name']);
            $img->fit(640, 480)
                ->save(public_path() . '/assets/images/uploads/posts/' . $_FILES['image']['name']);
            $image = '/assets/images/uploads/posts/' . $_FILES['image']['name'];
        }

        $post = Post::withTrashed()
            ->where('id', $id)->update([
            'caption' => $this->request->caption,
            'announce' => json_encode($announce),
            'detail' => html_entity_decode($this->request->detail),
            'image' => $image,
            'is_visible' => $this->request->is_visible,
        ]);

        if ($post){
            return back()->with('message', trans('messages.post_saved'));
        } else {
            return back();
        }
    }

    /**
     * Удаление статьи
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Post::where('id', $id)->delete();

        return redirect()->route('admin.post.all')->with('message', trans('messages.post_deleted'));
    }


    /**
     * Восстановление удалённой статьи
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function recover($id)
    {
        Post::where('id', $id)->restore();

        return redirect()->back()->with('message', trans('messages.post_recovered'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CommentController extends Controller
{
    public function addWithAjax()
    {
        $comment = new Comment();

        $comment->text = $_GET['comment'];
        $comment->user_id = $_GET['user_id'];
        $comment->post_id = $_GET['post_id'];
        $comment->save();

        $_GET['created_at'] = date("d.m.Y H:i:s", strtotime($comment["created_at"]));
        $_GET['image'] = $comment->user->avatar;
        $_GET['id'] = $comment->id;
        $_GET['del_btn'] = 1;

        echo json_encode($_GET);
    }

    public function deleteWithAjax()
    {
        $comment = Comment::find($_GET['id']);
        if (Gate::allows('delete-comment', $comment)){
            $comment->delete();
        }
    }
}

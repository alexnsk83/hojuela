<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        $title = 'Галерея';
        $galleries = Gallery::all();

        return view('pages.galleries', compact('title', 'galleries'));
    }

    public function show($id)
    {
        $title = 'Галерея';
        $gallery = Gallery::findOrFail($id)->first();
        $photos = $gallery->photos->sortBy('order');

        return view('pages.gallery', compact('title', 'gallery', 'photos'));
    }
}

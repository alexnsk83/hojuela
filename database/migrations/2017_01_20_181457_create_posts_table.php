<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('caption');
            $table->string('image')->nullable();
            $table->text('announce');
            $table->text('detail');
            $table->integer('views')->default(0)->unsigned();
            $table->tinyInteger('is_visible')->nullable()->unsigned();
            $table->tinyInteger('show_comments')->nullable()->unsigned();
            $table->tinyInteger('commentable')->nullable()->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

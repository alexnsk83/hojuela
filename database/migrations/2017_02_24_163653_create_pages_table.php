<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('module_id');
            $table->tinyInteger('menu_1')->default(1)->nullable();
            $table->tinyInteger('menu_2')->default(1)->nullable();
            $table->string('slug')->nullable();
            $table->string('keywords')->nullable();
            $table->string('description')->nullable();
            $table->text('text')->nullable();
            $table->integer('level')->default(0);
            $table->integer('parent_id')->default(0);
            $table->integer('sort_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for ($i = 0; $i < 10; $i++) {
            Post::create([
                'caption' => $faker->realText(40),
                'image' => $faker->imageUrl(640,480),
                'announce' => json_encode($faker->realText(100)),
                'detail' => $faker->realText(512),
                'user_id' => 1,
                'is_visible' => 1,
                'show_comments' => 1,
                'commentable' => 1,
            ]);
        }
    }
}

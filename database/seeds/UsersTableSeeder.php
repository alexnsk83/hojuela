<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminName = 'admin';
        $adminMail = 'admin@admin.ru';
        $adminPass = 'admin';
        $adminAvatar = '/assets/images/uploads/avatars/user_avatar_empty.png';

        User::create([
            'name' => $adminName,
            'email' => $adminMail,
            'avatar' => $adminAvatar,
            'password' => bcrypt($adminPass),
            'status' => 1,
        ]);
    }
}

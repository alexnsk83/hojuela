$(function () {
    //События
    $('.submit').on('click', function () {
        $(this).attr('disabled', true);
        if($('.comment_text').val()){
            addCommentHandler();
        }
        $(this).attr('disabled', false);
    });

    $('.comment_text').keypress(function (e) {
        if(e.keyCode == 13) {
            if($('.comment_text').val()){
                addCommentHandler();
            }
            e.preventDefault();
        }
    });

    $('.comment-list').on("click", '.comment_delete', function () {
        var comment = $(this).parents('.comment_block').attr('data-id');
        $.ajax({
            url: 'comment/delete',
            type: 'GET',
            data: { id: comment }
        });
        $(this).parents('.comment_block').slideUp();
    });


    //Функции
    function addCommentHandler() {
        $.ajax({
            url: 'comment/add',
            type: 'GET',
            data: $('#add_comment').serialize(),
            success: addComment
        });
        $('.comment').val('');
    }

    function addComment(response) {
        response = JSON.parse(response);
        if (response['del_btn']){
            response['del_btn'] = '<span class="del_btn" href="#" style="display: inline-block"><img class="comment_delete" src="/assets/images/trash.png" alt="Delete" title="Удалить комментарий"> </span>'
        }
        var content = '<div data-id="' + response['id'] + '" class="comment_block comment clearfix last_comment ">' +
            '<div class="comment-avatar pull-left">' +
            '<img src="' + response['image'] + '" alt="User Avatar" class="img-circle comment-avatar-image">' +
            '</div>' +
            '<div class="comment-body clearfix">' +
            '<div class="comment-header">' +
            '<strong class="primary-font">' + response['user_name'] + '</strong>' +
            '<small class="pull-right text-muted">' +
            response["del_btn"] +
            '<span class="glyphicon glyphicon-time"></span>' + response['created_at'] +
            '</small>' +
            '</div>' +
            '<p class="comment-text">'+ response['comment'] +'</p>' +
            '</div>' +
            '</div>';

        $('.comment-list').append(content);
        $('.last_comment').last().hide();
        $('.last_comment').last().slideDown();

        $('.comment_text').val('');
    }
});

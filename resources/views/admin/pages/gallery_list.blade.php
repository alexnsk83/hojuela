@extends('admin.base')

@section('content')
    <section class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('admin.gallery.add') }}" class="btn btn-primary margin-top">{{ trans('site.button.gallery_new') }}</a>
                @if ($trashed)
                    <a href="{{ route('admin.gallery.all') }}" class="btn btn-primary margin-top">{{ trans('site.button.gallery_active') }}</a>
                @else
                    <a href="{{ route('admin.gallery.trashed') }}" class="btn btn-primary margin-top">{{ trans('site.button.gallery_trashed') }}</a>
                @endif
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @foreach($galleries as $gallery)
                    <div class="box">
                        <div class="row">
                            <div class="col-xs-4 col-sm-2">
                                @if ($gallery['cover_image'])
                                    <img class="wp-post-image" src="{{ $gallery['cover_image'] }}" alt="image">
                                @endif
                            </div>
                            <div class="col-xs-8 col-sm-8">
                                <h3>{{ $gallery['name'] }}</h3>
                                <p>{!! json_decode($gallery['description']) !!}</p>
                            </div>
                            <div class="col-xs-12 col-sm-2">
                                <div class="control-buttons">
                                    <a class="admin-ctrl-btn admin-btn fa fa-pencil-square-o" title="{{ trans('site.button.gallery_edit') }}"
                                       href="{{ route('admin.gallery.edit', ['id' => $gallery['id']]) }}"></a>
                                    @if ($gallery->trashed())
                                        <a class="admin-ctrl-btn admin-btn fa fa-magic" title="{{ trans('site.button.gallery_recover') }}"
                                           href="{{ route('admin.gallery.recover', ['id' => $gallery['id']]) }}"></a>
                                    @else
                                        <a class="admin-ctrl-btn admin-btn fa fa-trash-o" title="{{ trans('site.button.gallery_delete') }}"
                                           href="{{ route('admin.gallery.delete', ['id' => $gallery['id']]) }}"
                                           onclick="return confirm('{{ trans('site.dialog.delete_gallery') }}')"></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
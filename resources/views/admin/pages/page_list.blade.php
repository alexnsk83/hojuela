@extends('admin.base')

@section('content')
    <section class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('admin.page.add') }}" class="btn btn-primary margin-top">{{ trans('site.button.page_new') }}</a>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @foreach($pages as $page)
                    <div class="row menu_list-item">
                        <div class="box">
                            <div class="col-xs-12 col-sm-9">
                                <h4>{{ $page['name'] }}</h4>
                                <a class="admin-ctrl-btn admin-btn fa fa-pencil-square-o" title="{{ trans('site.button.page_edit') }}"
                                   href="{{ route('admin.page.edit', ['id' => $page['id']]) }}"></a>
                                <a class="admin-ctrl-btn admin-btn fa fa-trash-o" title="{{ trans('site.button.page_delete') }}"
                                   href="{{ route('admin.page.delete', ['id' => $page['id']]) }}"
                                   onclick="return confirm('{{ trans('site.dialog.sure') }}')"></a>
                                <span class="admin-btn">
                                @if ($page['header'])
                                        <span class="published">В шапке</span>
                                @endif
                                </span>
                                <span class="admin-btn">
                                @if ($page['footer'])
                                        <span class="published">В подвале</span>
                                @endif
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
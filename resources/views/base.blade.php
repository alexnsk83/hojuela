<!DOCTYPE html>
<html lang="ru">
    <head>
        @include('blocks.head')
    </head>
    <body>
        @include('blocks.header')
        <div class="container">
            <div class="row">
                @section('content')
                @show
                @include('blocks.right')
            </div>
        </div>
        @include('blocks.footer')
        @include('blocks.copyright')
        @include('blocks.scripts')

    </body>
</html>
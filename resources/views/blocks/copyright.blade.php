<footer class="copyrights">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="copyrights--right">
                    <a href="https://vk.com/id8785339" target="_blank">
                        Hojuela-CMS by Alexey Balobanov © Copyright {{ date('Y') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
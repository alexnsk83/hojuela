<footer class="footer">
    <div class="container">
                <div class="col-xs-12  col-md-6">
            <nav class="widget-navigation  push-down-30">
                <h6>Навигация</h6>
                <hr>
                <ul class="navigation">
                    <li> <a href="/">{{ trans('site.button.main') }}</a> </li>
                    @if (Auth::check() && Auth::user()->status === 1)
                        <li class=""><a href="{{ route('admin.home') }}" class="dropdown-toggle" data-toggle="dropdown">{{ trans('site.button.admin') }}</a></li>
                    @endif
                    @if (Auth::check())
                        <li><a href="{{ route('site.auth.logout') }}">{{ trans('site.button.logout') }}</a></li>
                    @else
                        <li><a href="{{ route('site.auth.login') }}">{{ trans('site.button.login') }}</a></li>
                    @endif
                </ul>
            </nav>
        </div>
        <div class="col-xs-12  col-md-6">
            <div class="widget-contact  push-down-30">
                <h6>Контакты</h6>
                <hr>
                <span class="widget-contact__text">
                    <span class="widget-contact__title">Иванов Иван Иванович</span>
                    <br>Email: ivan@ivanov.ru
                    <br>Skype: ivanovivan
                    <br>VK: https://vk.com/ivanovoficial
                </span>
            </div>
        </div>
    </div>
</footer>
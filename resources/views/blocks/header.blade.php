<header class="header">
    <div class="container very-top-menu">
        <nav class="navbar navbar-default pull-right" role="navigation">
            <div>
                <ul class="navigation pull-right">
                    <li>
                        @if (Auth::check())
                            <div class="greeting dropdown-toggle">{{ trans('auth.greet') }} {{ Auth::user()->name }}</div>
                        @endif
                    </li>
                    @if (Auth::check() && Auth::user()->status === 1)
                        <li><a href="{{ route('admin.home') }}" class="dropdown-toggle" data-toggle="dropdown">{{ trans('site.button.admin') }}</a></li>
                    @endif
                    @if (Auth::check())
                        <li><a href="{{ route('site.auth.logout') }}" class="dropdown-toggle" data-toggle="dropdown">{{ trans('site.button.logout') }}</a></li>
                    @else
                        <li><a href="{{ route('site.auth.login') }}" class="dropdown-toggle" data-toggle="dropdown">{{ trans('site.button.login') }}</a></li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="header_image">
                <a href="{{ route('site.home') }}">
                    <img src="/assets/images/logo.png">
                </a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#readable-navbar-collapse2">
                <span class="sr-only">Toggle navigation</span>
                <span class="glyphicon glyphicon-menu-hamburger"></span>
            </button>
        </div>
        <nav class="navbar-main navbar-default" role="navigation">
            <div class="collapse  navbar-collapse" id="readable-navbar-collapse2">
                <ul class="navigation">
                    @if (count($menu_1) > 0)
                        @foreach ($menu_1 as $item) <!-- Получаем из AppServiceProvider -->
                        <li class="col-xs-12 col-sm-2">
                            @if ($item->module_id === 1)
                                <a href="{{ route($item->module->url, $item['slug']) }}">{{ $item['name'] }}</a>
                            @else
                                <a href="{{ route($item->module->url) }}">{{ $item['name'] }}</a>
                            @endif
                        </li>
                        @endforeach
                    @else
                        @if (Auth::check() && Auth::user()->status === 1)
                        <li class="col-xs-12">
                            <h5>Не создано ни одного пункта меню</h5>
                        </li>
                        @endif
                    @endif
                </ul>
            </div>
        </nav>
    </div>
</div>

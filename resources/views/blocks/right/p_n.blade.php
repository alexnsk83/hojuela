<div class="widget-posts  push-down-30">
    <h6>Популярное / Свежее</h6>

    <ul class="nav  nav-tabs">
        <li class="active">
            <a href="#recent-posts" data-toggle="tab"> <span class="glyphicon  glyphicon-time"></span> &nbsp;Свежо</a>
        </li>
        <li>
            <a href="#popular-posts" data-toggle="tab"> <span class="glyphicon  glyphicon-flash"></span> &nbsp;Популярно </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane  fade  in  active" id="recent-posts">
            @php $i = 0; @endphp
            @foreach ($latestPosts as $post)
                <div class="push-down-15">
                    @if (!empty($post['image']))
                        <div class="preview"><img class="wp-post-image" src="{{ $post['image'] }}" alt="Blog image" width="30px"></div>
                    @endif
                    <h5>
                        <a href='{{ route('site.post.show', $post['id']) }}'> {{$post['caption']}}</a>
                    </h5>
                    <span class="widget-posts__time">{{ $post['created_at']->diffForHumans() }}</span>
                </div>

            @endforeach
        </div>

        <div class="tab-pane  fade" id="popular-posts">
            @php $i = 0; @endphp
            @foreach ($popularPosts as $post)
                <div class="push-down-15">
                    @if (!empty($post['image']))
                        <div class="preview"><img class="wp-post-image" src="{{ $post['image'] }}" alt="Blog image" width="30px"></div>
                    @endif
                    <h5>
                        <a href='{{ route('site.post.show', $post['id']) }}'>{{$post['caption']}}</a>
                    </h5>
                    <span class="widget-posts__time">{{ $post['created_at']->diffForHumans() }}</span>
                </div>
            @endforeach
        </div>
    </div>
</div>
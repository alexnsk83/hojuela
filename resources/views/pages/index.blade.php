@extends('base')

@section('content')
    <div class="col-xs-12  col-md-8">
        @foreach ($posts as $post)
            @if ($post['is_visible'])
                <div class="boxed  push-down-45">
                    <div class="row">
                        <div class="col-lg-4">
                            @if ($post['image'])
                                <img class="wp-post-image" src="{{ $post['image'] }}" alt="Blog image">
                            @endif
                        </div>
                        <div class="col-lg-8">
                            <h2 class="front-page-title"><a href='{{ route('site.post.show', $post['id']) }}'>{{$post['caption']}}</a></h2>
                            <p>{!! json_decode($post['announce']) !!}</p>
                            <div class="row">
                                <div class="col-xs-12  col-sm-8">
                                    <div class="meta__info">
                                        <a href="#">Автор: {{ $post->user->name or "" }}</a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="meta__comments pull-right">
                                        <span class="meta__date"><span class="glyphicon glyphicon-calendar"></span>{{ $post['created_at']->diffForHumans() }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="read-more"><a class="read-on" href='{{ route('site.post.show', $post['id']) }}'>{{ trans('site.button.read_more') }} <span class="glyphicon glyphicon-chevron-right"></span></a>
                                <div class="comment-icon-counter">
                                    <span class="glyphicon glyphicon-comment comment-icon"></span>
                                    <span class="comment-counter">{{ $post->comments->count() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        {{ $posts->render() }}
    </div>

@endsection
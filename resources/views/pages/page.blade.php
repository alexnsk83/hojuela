@extends('single_row')

@section('content')
    <div class="boxed  push-down-60">
        <div class="row">
            <div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">
                <div class="post-content">
                    <h1>{{ $page['name'] }}</h1>
                    <p> {!! $page['text'] !!} </p>
                </div>
                <div class="row">
                    <div class="col-xs-12  col-sm-6">
                        <div class="social-icons">
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,lj,telegram"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('single_row')

@section('content')
<div class="boxed  push-down-60">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">
            <div class="post-content">
                <h1>Регистрация</h1>
                <form enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    <input type="text" name="name" placeholder="{{ trans('site.form.name') }}" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                        <span class="red">{{ $errors->get('name')[0] }} </span>
                        @endif <br>
                    <input type="password" name="password" placeholder="{{ trans('site.form.password') }}">
                        @if ($errors->has('password'))
                        <span class="red">{{ $errors->get('password')[0] }} </span>
                        @endif<br>
                    <input type="password" name="password2" placeholder="{{ trans('site.form.password_repeat') }}">
                        @if ($errors->has('password2'))
                            <span class="red">{{ $errors->get('password2')[0] }} </span>
                        @endif<br>
                    <input type="email" name="email" placeholder="{{ trans('site.form.email') }}" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                        <span class="red">{{ $errors->get('email')[0] }} </span>
                        @endif<br>
                    <span>Аватар</span>
                    @if ($errors->has('avatar'))
                        <span class="red">{{ $errors->get('avatar')[0] }} </span>
                    @endif<br>
                    <input type="file" name="avatar"><br>
                    <input type="submit" name="reg" value="{{ trans('site.button.register') }}"><br>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
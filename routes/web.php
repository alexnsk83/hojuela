<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get( '/', 'PostController@showAll')->name('site.home');

Route::get('/post/{id}', 'PostController@showPost')->name('site.post.show');
Route::post('/post/{id}', 'PostController@addComment')->name('site.post.show');

Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', 'AuthController@register')->name('site.auth.register');
    Route::post('/register', 'AuthController@registerPost')->name('site.auth.registerPost');

    Route::get('/login', 'AuthController@login')->name('site.auth.login');
    Route::post('/login', 'AuthController@loginPost')->name('site.auth.loginPost');
});

Route::get('/logout', 'AuthController@logout')->name('site.auth.logout');

Route::get('/post/comment/add', 'CommentController@addWithAjax');
Route::get('/post/comment/delete', 'CommentController@deleteWithAjax');

Route::get('/shop', 'ShopController@show')->name('site.shop');

Route::get('/gallery', 'GalleryController@index')->name('site.gallery');
Route::get('/gallery/{id}', 'GalleryController@show')->name('site.gallery.show');

Route::get('/page/{slug}', 'PageController@showPage')->name('site.page');

//Admin routes
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'isAdmin'], function () {
    Route::get('/', 'AdminController@index')->name('admin.home');

    //Post
    Route::group(['prefix' => 'post'], function () {
        Route::get('/', 'PostController@showAll')->name('admin.post.all');
        Route::get('/trashed', 'PostController@showTrashed')->name('admin.post.trashed');

        Route::get('/add', 'PostController@add')->name('admin.post.add');
        Route::post('/add', 'PostController@addPost')->name('admin.post.addPost');

        Route::get('/edit/{id}', 'PostController@edit')->name('admin.post.edit')->where('id', '[0-9]+');
        Route::post('edit/{id}', 'PostController@editPost')->name('admin.post.edit')->where('id', '[0-9]+');

        Route::get('/delete/{id}', 'PostController@delete')->name('admin.post.delete')->where('id', '[0-9]+');
        Route::get('/recover/{id}', 'PostController@recover')->name('admin.post.recover')->where('id', '[0-9]+');
    });

    //User
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@showAll')->name('admin.user.all');

        Route::get('/edit/{id}', 'UserController@edit')->name('admin.user.edit')->where('id', '[0-9]+');
        Route::post('/edit/{id}', 'UserController@editPost')->name('admin.user.editPost')->where('id', '[0-9]+');

        Route::get('/delete/{id}', 'UserController@delete')->name('admin.user.delete');
        Route::get('/delete/{id}', 'UserController@delete')->name('admin.user.delete');
    });

    //Page
    Route::group(['prefix' => 'page'], function () {
        Route::get('/all', 'PageController@show')->name('admin.page.all');
        Route::get('/add', 'PageController@add')->name('admin.page.add');
        Route::post('/add', 'PageController@addPost')->name('admin.page.addPost');

        Route::get('/edit/{id}', 'PageController@edit')->name('admin.page.edit');
        Route::post('/edit/{id}', 'PageController@editPost')->name('admin.page.edit');

        Route::get('/delete/{id}', 'PageController@delete')->name('admin.page.delete');
    });

    //Gallery
    Route::group(['prefix' => 'gallery'], function () {
        Route::get('/', 'GalleryController@index')->name('admin.gallery.all');
        Route::get('/trashed', 'GalleryController@showTrashed')->name('admin.gallery.trashed');

        Route::get('/add', 'GalleryController@add')->name('admin.gallery.add');
        Route::post('/add', 'GalleryController@addPost')->name('admin.gallery.add');

        Route::get('/edit/{id}', 'GalleryController@edit')->name('admin.gallery.edit')->where('id', '[0-9]+');
        Route::post('edit/{id}', 'GalleryController@editPost')->name('admin.gallery.edit')->where('id', '[0-9]+');

        Route::get('/delete/{id}', 'GalleryController@delete')->name('admin.gallery.delete')->where('id', '[0-9]+');
        Route::get('/recover/{id}', 'GalleryController@recover')->name('admin.gallery.recover')->where('id', '[0-9]+');

        Route::post('photos/change-order', 'GalleryController@changeOrderWithAjax')->name('admin.photo.order');
    });


});

